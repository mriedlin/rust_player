Michael V Riedlin
A simple video player written in Rust

I wrote this mostly as a way to learn more about Rust. It is
likely a poor example of Rust idioms and should really only
be considered as a toy.

Building:
Without modifications (and assuming I don't commit libraries),
you will need to supply your own build of FFMPEG. I used
FFMPEG commit: 50ce510ac4e3ed093c051738242a9a75aeeb36ce

I configured ffmpeg with:
--enable-pic
--disable-static
--enable-shared
--disable-programs
--disable-doc

Once compiled, copy the dynamics (the .so and .so.version) into rust_player/extlib.
Alternatively, you could probably link against an installed ffmpeg provided by your
distribution.  To do that you would need to alter build.rs. Cargo's documentation
has more on that.

Once you have that, running make in the shim directory should setup the shim library
for ffmpeg that I made for this project.

cargo build should then get you the rest of the way.

To run the program, you'll need to let the loader know about extlib:
export LD_LIBRARY_PATH=/some/path/to/extlib:$LD_LIBRARY_PATH

Usage:
$./rust_player path-to-video

License:
See LICENSE.txt
