extern crate piston_window;
use std::*;

/*
 * Break this out once done
*/
extern crate libc;

// Opaque for as long as I can get away with
enum AVFormatContext {}
enum AVCodecContext {}
enum AVInputFormat {}
enum AVDictionary {}
enum AVCodec {}
enum AVFrame {}
enum AVBufferRef {}
enum AVPacketSideData {}

enum AVMediaType{
    AVMEDIA_TYPE_UNKNOWN,
    AVMEDIA_TYPE_VIDEO,
    AVMEDIA_TYPE_AUDIO,
    AVMEDIA_TYPE_DATA,
    AVMEDIA_TYPE_SUBTITLE,
    AVMEDIA_TYPE_ATTACHMENT,
    AVMEDIA_TYPE_NB, 
}

enum AVDiscard {
    AVDISCARD_NONE,
    AVDISCARD_DEFAULT,
    AVDISCARD_NONREF,
    AVDISCARD_BIDIR,
    AVDISCARD_NONINTRA,
    AVDISCARD_NONKEY,
    AVDISCARD_ALL,
}

#[repr(C)]
struct AVPacket {
    buf: *mut AVBufferRef,
    pts: libc::int64_t,
    dts: libc::int64_t,
    data: *mut libc::uint8_t,
    size: libc::c_int,
    stream_index: libc::c_int,
    flags: libc::c_int,
    side_data: *mut AVPacketSideData,
    side_data_elems: libc::c_int,
    duration: libc::int64_t,
    pos: libc::int64_t,
    convergence_duration: libc::int64_t, // deprecated
}

#[repr(C)]
struct AVRational {
    num: libc::c_int,
    den: libc::c_int,
}

#[repr(C)]
struct AVStream {
    index: libc::c_int,
    id: libc::c_int,
    codec: *mut AVCodecContext,
    priv_data: *mut libc::c_void,
    time_base: AVRational,
    start_time: libc::int64_t,
    duration: libc::int64_t,
    nb_frames: libc::int64_t,
    disposition: libc::c_int,
    discard: AVDiscard,
    sample_aspect_ratio: AVRational,
    attached_pic: AVPacket,
    side_data: *mut AVPacketSideData,
    nb_side_data: libc::c_int,
    event_flags: libc::c_int,
}

// declare externs. linkage specified by cargo, see build.rs at root
extern {
    // AVFormat functions
    fn av_register_all();

    fn avformat_open_input(
        ps: *mut *mut AVFormatContext, 
        fname: *const libc::c_char, 
        fmt: *mut AVInputFormat, 
        options: *mut *mut AVDictionary) -> libc::c_int;

    fn av_read_frame(s: *mut AVFormatContext, pkt: *mut AVPacket) -> libc::c_int;

    fn avformat_close_input(s: *mut *mut AVFormatContext);

    fn av_find_best_stream(
        ic: *mut AVFormatContext, 
        tp: AVMediaType, 
        wanted_stream_nb: libc::c_int, 
        related_stream: libc::c_int, 
        decoder_ret: *mut *mut AVCodec,
        flags: libc::c_int,
    ) -> libc::c_int;

    fn avcodec_decode_video2(
        avctx: *mut AVCodecContext,
        picture: *mut AVFrame,
        got_picture_ptr: *mut libc::c_int,
        avpkt: *const AVPacket,
    ) -> libc::c_int;
}

struct Context {
    ctx: *mut AVFormatContext,
    dec: *mut AVCodec,
    pkt: *mut AVPacket,
    idx: i32,
}

impl Context {
    fn new(fname: String) -> Result<Context, String> {
        let cstr = ffi::CString::new(fname.into_bytes()).unwrap();
        let mut ctx = Context{
            ctx: ptr::null_mut(), 
            dec: ptr::null_mut(),
            pkt: ptr::null_mut(),
            idx: -1,
        };
        unsafe{
            let status = avformat_open_input(&mut ctx.ctx, cstr.as_ptr(), ptr::null_mut(), ptr::null_mut());
            if status == 1 {
                Ok(ctx)
            }
            else{
                let msg = "avformat_open_input failed with: ".to_string();
                let num = status.to_string();
                Err(msg + &num)
            }
        }
    }

    fn set_decoder(&mut self, desired: AVMediaType) -> bool {
        unsafe{
            let stream_idx = av_find_best_stream(self.ctx, desired, -1, -1, &mut self.dec, 0);

            // Not sure this is altogether correct, but not willing to port over ffmpeg's defines
            if stream_idx >= 0 {
                self.idx = stream_idx;
                true
            }
            else {
                false
            }
        }
    }

    //fn get_frame(&mut self) -> bool {
    //    if self.idx < 0 {
    //        return false;
    //    }
    //    
    //    let res: i32 = -1;
    //    unsafe{
    //        loop {
    //            res = av_read_frame(self.ctx, self.pkt);
    //            if res < 0 || self.pkt == ptr::null_mut() {
    //                return false;
    //            }

    //            if self.pkt.stream_index == self.idx {
    //                res = avcodec_decode_video2(self.ctx, , , self.pkt);
    //                if res < 0 {
    //                    return false;
    //                }
    //                else if res > 0 {
    //                    return true;
    //                }
    //                // The part where I hope I don't loop forever...
    //            }
    //        }
    //    }
    //}
}

impl Drop for Context {
    fn drop(&mut self){
        println!("Context exiting scope");
        unsafe{
            avformat_close_input(&mut self.ctx);
        }
    }
}
/*
 * End section
*/

fn main() {
    println!("Starting rust player");
    println!("Registering libav");
    unsafe{
        av_register_all();
    }

    if env::args().count() != 2 {
        println!("Takes one argument");
        return;
    }

    let s: String;
    match env::args().last() {
        Some(p) => s = p,
        None => return,
    }

    let context = Context::new(s.clone());

    // This looks really fucking dumb -_-
    let dummy = s.clone();
    let path = path::Path::new(&dummy);
    let buf = s.into_bytes();
    let cfname = ffi::CString::new(buf).unwrap();


    let window_result = piston_window::WindowSettings::new(
        "Rust Player",
        [600, 400]
    )
    .exit_on_esc(true)
    .build();

    let window: piston_window::PistonWindow;
    match window_result {
        Err(s) => {
            println!("Error building window: {}", s);
            return;
        },
        Ok(w) => window = w,
    }

    let image = piston_window::Texture::from_path(
        &mut *window.factory.borrow_mut(),
        &path,
        piston_window::Flip::None,
        &piston_window::TextureSettings::new()
    ).unwrap();

    for e in window {
        e.draw_2d(|c,g| {
            piston_window::clear([0.0, 0.0, 1.0, 1.0], g);
            piston_window::image(&image, c.transform, g);
        });
    }
}
