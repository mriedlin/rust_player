extern crate piston_window;
extern crate image;
extern crate libc;

use std::*;

// The shim's struct will just be opaque, much easier on me
enum ffmpeg_bundle {}

// Wrap just this one thing, it's small
#[repr(C)]
enum AVMediaType{
    AVMEDIA_TYPE_UNKNOWN,
    AVMEDIA_TYPE_VIDEO,
    AVMEDIA_TYPE_AUDIO,
    AVMEDIA_TYPE_DATA,
    AVMEDIA_TYPE_SUBTITLE,
    AVMEDIA_TYPE_ATTACHMENT,
    AVMEDIA_TYPE_NB, 
}

// wrap up the shim
extern {
    fn ffmpeg_init();

    fn ffmpeg_create_bundle() -> *mut ffmpeg_bundle;

    fn ffmpeg_get_context(bndl: *mut ffmpeg_bundle, fname: *const libc::c_char, width: libc::int32_t, height: libc::int32_t) -> libc::c_int;

    fn ffmpeg_finish_bundle(bndl: *mut ffmpeg_bundle);

    fn ffmpeg_best_stream(bndl: *mut ffmpeg_bundle, t: AVMediaType) -> libc::c_int;

    fn ffmpeg_get_frame(bndl: *mut ffmpeg_bundle) -> libc::c_int;

    fn ffmpeg_convert_frame(bndl: *mut ffmpeg_bundle) -> libc::c_int;

    fn ffmpeg_deliver_frame(bndl: *mut ffmpeg_bundle, buf: *mut u8, sz: libc::int32_t);
}

fn main() {
    println!("Starting rust player");
    println!("Registering libav");

    let bundle: *mut ffmpeg_bundle;
    unsafe{
        ffmpeg_init();
        bundle = ffmpeg_create_bundle();
    }

    if env::args().count() != 2 {
        println!("Takes one argument");
        return;
    }

    let s: String;
    match env::args().last() {
        Some(p) => s = p,
        None => return,
    }

    println!("Start big unsafe block");

    let buf = s.into_bytes();
    let cfname = ffi::CString::new(buf).unwrap();


    println!("Survived big unsafe block");

    let window_result = piston_window::WindowSettings::new(
        "Rust Player",
        [600, 400]
    )
    .exit_on_esc(true)
    .build();

    let window: piston_window::PistonWindow;
    match window_result {
        Err(s) => {
            println!("Error building window: {}", s);
            return;
        },
        Ok(w) => window = w,
    }

    unsafe{
        let status = ffmpeg_get_context(bundle, cfname.as_ptr(), 600, 400);
        if status == -1 {
            ffmpeg_finish_bundle(bundle);
            return;
        }
        let status = ffmpeg_best_stream(bundle, AVMediaType::AVMEDIA_TYPE_VIDEO);
        if status < 0 {
            ffmpeg_finish_bundle(bundle);
            return;
        }
    }

    let mut v = Vec::with_capacity(600 * 400 * 4);
    unsafe{
        v.set_len(600 * 400 * 4);
    }
    let opt_img_buf = image::ImageBuffer::from_raw(600, 400, v).unwrap();
    let mut texture = piston_window::Texture::from_image(
        &mut *window.factory.borrow_mut(),
        &opt_img_buf,
        &piston_window::TextureSettings::new()
    ).unwrap();

    for e in window {
        e.draw_2d(|c,g| {
            piston_window::clear([0.0, 0.0, 1.0, 1.0], g);
            thread::sleep(time::Duration::from_millis(33));
            let raw_img: Vec<u8>;
            unsafe {
                println!("entered unsafe block in draw thread");
                let status = ffmpeg_get_frame(bundle);
                if status < 0 {
                    println!("ffmpeg_get_frame failed");
                    ffmpeg_finish_bundle(bundle);
                    return;
                }
                println!("attempting to deliver frame");
                let sz = ffmpeg_convert_frame(bundle);
                let mut v = Vec::with_capacity(sz as usize);
                v.set_len(sz as usize);
                ffmpeg_deliver_frame(bundle, v.as_mut_ptr(), sz);
                raw_img = v;
                println!("Made a vector");
            }
            println!("survived unsafe block");

            let opt_img_buf = image::ImageBuffer::from_raw(600, 400, raw_img);
            match opt_img_buf {
                Some(b) => {
                    texture.update(
                        &mut *e.factory.borrow_mut(),
                        &b
                    ).unwrap();
                    piston_window::image(&texture, c.transform, g);
                },
                None => println!("Failed to convert from raw"),
            }
        });
    }

    println!("left the loop");

    unsafe{
        ffmpeg_finish_bundle(bundle);
    }
}
