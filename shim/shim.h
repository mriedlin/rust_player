#ifndef RUST_PLAYER_FFMPEG_SHIM
#define RUST_PLAYER_FFMPEG_SHIM

#include <stdint.h>

#include <libavformat/avformat.h>
#include <libavcodec/avcodec.h>
#include <libswscale/swscale.h>

typedef struct ffmpeg_bundle{
  int error;
  enum AVMediaType type;
  AVFormatContext* ctx;
  int32_t width;
  int32_t height;
  int64_t pts;
  int stream_index;
  AVFrame* frame;
  struct SwsContext* scale_ctx;
  uint8_t* out_buf;
  int32_t out_buf_sz;
} ffmpeg_bundle;

void ffmpeg_init();

ffmpeg_bundle* ffmpeg_create_bundle();

int ffmpeg_get_context(ffmpeg_bundle* bndl, const char* fname, int32_t width, int32_t height);

void ffmpeg_finish_bundle(ffmpeg_bundle* bndl);

int ffmpeg_best_stream(ffmpeg_bundle* bndl, enum AVMediaType type);

int ffmpeg_get_frame(ffmpeg_bundle* bndl);

int ffmpeg_convert_frame(ffmpeg_bundle* bndl);

#endif
