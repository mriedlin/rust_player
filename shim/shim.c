// FFMPEG shim, so I don't have to reexport ALL of ffmpeg
// for the small slice of functionality I want
#include "shim.h"

#include <libavutil/imgutils.h>

#include <stdio.h>

static char errmsg[256];

void ffmpeg_init(){
  av_register_all();
}

ffmpeg_bundle* ffmpeg_create_bundle(){
  return (ffmpeg_bundle*) malloc(sizeof(ffmpeg_bundle));
}

int ffmpeg_get_context(ffmpeg_bundle* bndl, const char* fname, int32_t width, int32_t height){
  AVFormatContext* ctx = NULL;
  bndl->error = avformat_open_input(&ctx, fname, NULL, NULL);
  if(bndl->error != 0){
    avformat_close_input(&ctx);
    return -1;
  }
  bndl->frame = NULL;
  bndl->scale_ctx = NULL;
  bndl->width = width;
  bndl->height = height;
  bndl->out_buf = NULL;
  bndl->out_buf_sz = 0;

  bndl->ctx = ctx;
  return 0;
}

void ffmpeg_finish_bundle(ffmpeg_bundle* bndl){
  fprintf(stdout,"ffmpeg_finish_bundle\n");
  av_frame_free(&(bndl->frame));
  sws_freeContext(bndl->scale_ctx);
  avformat_close_input(&(bndl->ctx));
  free(bndl);
}

int ffmpeg_best_stream(ffmpeg_bundle* bndl, enum AVMediaType type){
  bndl->type = type;
  //int ret = av_find_best_stream(bndl->ctx, type, -1, -1, NULL, 0);
  int ret = av_find_best_stream(bndl->ctx, AVMEDIA_TYPE_VIDEO, -1, -1, NULL, 0);
  if(ret < 0){
    bndl->error = ret;
  }
  else{
    bndl->stream_index = ret;
    AVCodecContext* dec_ctx = bndl->ctx->streams[ret]->codec;
    AVCodec* dec = avcodec_find_decoder(dec_ctx->codec_id);
    if(dec == NULL){
      fprintf(stdout,"Didn't get decoder\n");
      return -1;
    }
    ret = avcodec_open2(dec_ctx, dec, NULL);
    if(ret < 0){
      fprintf(stdout,"Failed to open\n");
      return ret;
    }
  }
  return ret;
}

int ffmpeg_process_frame(ffmpeg_bundle* bndl, AVPacket* pkt){
  if(bndl->frame == NULL){
    bndl->frame = av_frame_alloc();
  }
  else{
    //av_frame_unref(bndl->frame);
  }

  AVCodecContext* codec = bndl->ctx->streams[bndl->stream_index]->codec;
  int got_frame = 0; 
  int result = avcodec_decode_video2(codec, bndl->frame, &got_frame, pkt);
  if(got_frame){
    fprintf(stdout,"got frame\n");
    fprintf(stdout,"width: %i, height: %i\n", bndl->frame->width,bndl->frame->height);
    if(bndl->frame->key_frame){
      fprintf(stdout,"key frame\n");
    }
  }
  fprintf(stdout,"avcodec_decode_video2: %i\n", result);
  if(result <= 0){
    bndl->error = result;
    memset(&errmsg, 0, sizeof(errmsg));
    av_strerror(result, (char*)&errmsg, sizeof(errmsg));
    fprintf(stdout, "Error: %s\n",errmsg);
  }
  return result;
}

int ffmpeg_get_frame(ffmpeg_bundle* bndl){
  AVPacket pkt;
  av_init_packet(&pkt);
  int status = 0;
  do {
    int s = av_read_frame(bndl->ctx, &pkt);
    fprintf(stdout, "av_read_frame: %i\n", s);
    if(s != 0){
      bndl->error = s;
      return -1;
    }
    fprintf(stdout, "Desired stream: %i, packet stream: %i\n", bndl->stream_index, pkt.stream_index);
    if(pkt.stream_index == bndl->stream_index){
      fprintf(stdout, "ffmpeg_process_frame\n");
      status = ffmpeg_process_frame(bndl, &pkt);
    }
  } while(status == 0);
  av_packet_unref(&pkt);

  return status;
}

int ffmpeg_convert_frame(ffmpeg_bundle* bndl){
  if(bndl->scale_ctx == NULL){
    bndl->scale_ctx = sws_getContext(
      bndl->frame->width, 
      bndl->frame->height,
      bndl->ctx->streams[bndl->stream_index]->codec->pix_fmt,
      bndl->width,
      bndl->height,
      AV_PIX_FMT_RGBA,
      0, 0, 0, 0
    );
    if(bndl->scale_ctx == NULL){
      fprintf(stdout,"sws_getContext failed\n");
      return -1;
    }
  } 
  AVFrame* dst = av_frame_alloc();
  int num_bytes = av_image_alloc(dst->data, dst->linesize, bndl->width, bndl->height, AV_PIX_FMT_RGBA, 1);
  fprintf(stdout,"av_image_alloc allocated: %i bytes\n", num_bytes);
  if(num_bytes <= 0){
    fprintf(stdout,"av_image_alloc failed: %i\n", num_bytes);
    return -1;
  }

  int h = sws_scale(
    bndl->scale_ctx,
    (const uint8_t* const*) bndl->frame->data,
    bndl->frame->linesize,
    0,
    bndl->frame->height,
    dst->data,
    dst->linesize
  );
  if(h <= 0){
    return -1;
  }

  if(bndl->out_buf == NULL){
    fprintf(stdout,"Allocate out buffer\n");
    bndl->out_buf = (uint8_t*) malloc(num_bytes);
    bndl->out_buf_sz = num_bytes;
  }

  dst->width = bndl->width;
  dst->height = bndl->height;
  fprintf(stdout,"dst->width: %i\n",dst->width);
  fprintf(stdout,"dst->height: %i\n",dst->height);
  int res = av_image_copy_to_buffer(bndl->out_buf, bndl->out_buf_sz, (const uint8_t* const*) dst->data, dst->linesize, AV_PIX_FMT_RGBA, dst->width, dst->height, 1);
  if(res < 0){
    fprintf(stdout,"av_image_copy_to_buffer failed: %i\n", res);
    //free(out_buf);
    return res;
  }
  else{
    fprintf(stdout,"av_image_copy_to_buffer copied: %i\n", res);
  }
  //*pframe = out_buf;
  av_freep(&dst->data[0]);
  av_frame_free(&dst);
  return num_bytes;
}

void ffmpeg_deliver_frame(ffmpeg_bundle* bndl, uint8_t* buf, int32_t sz){
  memcpy(buf, bndl->out_buf, bndl->out_buf_sz);
}

void ffmpeg_delete_buf(uint8_t* buf){
  free(buf);
}
